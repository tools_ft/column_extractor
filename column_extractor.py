 # -*- coding: iso-8859-15 -*-
import os
import numpy as np
import pandas as pd
import re
import shutil
import click
from pathlib import Path
import csv


base_path = ""
output_not_processable_original = ""
output_processed_original = ""
output_processed_clean = ""


def create_folder(path):
    Path(path).mkdir(parents=True, exist_ok=True)



def set_paths(path):
    global base_path
    global output_not_processable_original
    global output_processed_original
    global output_processed_clean

    base_path = path
    output_not_processable_original = base_path + "\\not_processable_original"
    output_processed_original = base_path + "\\processed_original"
    output_processed_clean = base_path + "\\processed_clean"



@click.command()
@click.option('--path', default='.\\examples', help='The path of the input folder.')
def main(path):

    set_paths(path)

    # regex for valid email addresses (source: google)
    column_regex = re.compile('^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$')

    extensions_dict = {"text": ['csv', 'txt'], "excel": ['xls', 'xlsx']}

    to_be_processed_list = discover_files(extensions_dict)

    for data_file in to_be_processed_list:
        process_file(data_file,column_regex,extensions_dict)



def process_file(file_name,column_regex,extensions_dict):

    print(file_name)

    data_set_df,searched_column = open_data_file(file_name,column_regex,extensions_dict)

    file_processed = save_standard_data(data_set_df,searched_column, file_name) 
    
    move_original_file(file_name,file_processed)

    print('----------------------------------------------------')



def find_column(df_head,column_regex):

    searched_column = None
    
    for column in df_head:
        first_row_entry = str(df_head[column].iloc[0])

        if column_regex.match(first_row_entry):
            searched_column = column

    return searched_column



def get_extension(file_name):

    extension = file_name.rsplit('.')[-1]

    return extension.lower()



def rename_file_suffix(file_name):

    ext = get_extension(file_name)
    file_name = file_name.replace('.' + ext,'_standard.csv')

    return file_name



def save_file(clean_df,file_name):

    file_name = rename_file_suffix(file_name)

    saved_flag = False

    try:
        clean_file_path = output_processed_clean+'\\'+file_name

        if not os.path.isfile(clean_file_path):
            create_folder(output_processed_clean)
            clean_df.to_csv(clean_file_path,index=False,header=False)
            saved_flag = True
        else:
            print(clean_file_path,'\nAttention: File was already processed\n')
            saved_flag = True
    
    except Exception as e:
            print(str(e))

    return saved_flag



def move_original_file(file_name, file_processed):
    
    source = base_path + '\\' + file_name

    if file_processed:
        destination = output_processed_original + '\\' + file_name
        create_folder(output_processed_original)
    else: 
        destination = output_not_processable_original + '\\' + file_name
        create_folder(output_not_processable_original)
    
    try:    
        shutil.move(source,destination)
        # pass
    except Exception as e:
            print(str(e))



def header_found_in_excel(excel_file_dir,column_regex):

    try:
        excel_df = pd.read_excel(excel_file_dir, nrows = 1,header=None)
    except Exception as e:
            print(str(e))

    found_column = find_column(excel_df,column_regex)

    # in case we found the described column the header will be None else row number 0
    if found_column is not None:
        return None
    else:
        return 0



def open_excel(excel_file,column_regex):
    
    excel_file_dir = base_path + '\\' + excel_file
    excel_df = None
    searched_column = None

    header_found = header_found_in_excel(excel_file_dir,column_regex)

    try:
        excel_df = pd.read_excel(excel_file_dir, header = header_found)
        searched_column = find_column(excel_df.head(),column_regex)
    except Exception as e:
            print(str(e))

    return excel_df,searched_column



def detect_dialect(csv_file_dir,column_regex):
    """
    Returns the most possible separator in a csv file. In case ; and tab are not enough, you should add more prefered separators to the list below
    """
    with open(csv_file_dir,'r') as csv_file:
        sniffer = csv.Sniffer()
        try:
            dialect = sniffer.sniff(csv_file.readline(), delimiters=';\t')
            delimiter = dialect.delimiter
            quotechar = dialect.quotechar
        except Exception as e:
            print(str(e))
            delimiter = ';'
            quotechar = '"'

        header = header_found_in_csv(csv_file_dir,column_regex,delimiter,quotechar)

        return delimiter,quotechar,header



    
def header_found_in_csv(csv_file_dir,column_regex,delimiter,quotechar):
    try:
        csv_df = pd.read_csv(csv_file_dir, sep=delimiter,header=None,quotechar=quotechar,nrows=1,engine='python') 
    except Exception as e:
        print(str(e))
    
    #Opening file to check if there's a header (=if first row contains the specific regex then no header)
    found_column = find_column(csv_df,column_regex)

    # in case we found the described column the header will be None else row number 0
    if found_column is not None:
        return None
    else:
        return 0



def open_csv(csv_file,column_regex):

    csv_file_dir = base_path + '\\' + csv_file
    csv_df = None
    searched_column = None

    try:
        separator,quotechar,header = detect_dialect(csv_file_dir,column_regex)
        csv_df = pd.read_csv(csv_file_dir, sep=separator, quotechar=quotechar, header=header ,engine='python')
        searched_column = find_column(csv_df.head(),column_regex)
    
    except:
        print("Error reading file")


    return csv_df, searched_column




def open_data_file(file_name,column_regex,extensions):

    extension = get_extension(file_name)

    if extension in extensions["excel"]:
        data_set_df,searched_column = open_excel(file_name,column_regex)

    if extension in extensions["text"]:
        data_set_df,searched_column = open_csv(file_name,column_regex)

    return data_set_df,searched_column



def save_standard_data(data_set_df,searched_column,file_name):
    
    data_amount = len(data_set_df)
   
    try:
        
        clean_df = data_set_df[searched_column]
        
        try:
            file_processed_flag = save_file(clean_df,file_name)  

            if file_processed_flag:
                print('Processed ' + str(data_amount) + ' rows')

        except:
            file_processed_flag = False
            print(file_name,'\nError: File cannot not be saved.')

    except:
        file_processed_flag = False
        print('Error: No column detected with given format - lost ' + str(data_amount) + ' rows' )

    return file_processed_flag



def extensions_to_be_processed(extensions_dict):

    extensions_list = []

    for e_type in extensions_dict:
        for e in extensions_dict[e_type]:
            extensions_list.append(e)

    return extensions_list



def discover_files(extensions_dict):

    extensions_list = extensions_to_be_processed(extensions_dict)

    file_list = [c_file for c_file in os.listdir(base_path) if get_extension(c_file) in extensions_list]

    return file_list
           


if __name__ == "__main__":
    main()