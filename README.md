# Column extractor

Everyone has a point in their life when you have to extract a column from thousands of excel tables and csv files and throw away the rest.

No matter if your files are excel tables or csvs; having headers or not the script is going to extract exactly one column specified by a regular expression and save them as cleaned csv file.

You will have to pass a path argument which contains your files to be processed then depending on the file format each file is going to be cleaned while keeping the original or just saved in a different folder if not processable.

These folders are going to be created to sort the output files:

	not_processable_original: Containing the original unmodified files if the script hasn't recognised the formatting or doesn't contain the specified column.
	processed_original: Containing the original unmodified files after successful cleaning.
	processed_clean: Containing the cleaned files.

### Prerequisites

```
Python 3.8.2
click 7.1.1
pandas 1.0.3
```

### Installing

```
pip install click
pip install pandas

or

pip install -r requirements.txt
```